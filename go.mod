module gitlab.com/MikeTTh/smartcalc

go 1.12

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/andlabs/ui v0.0.0-20180902183112-867a9e5a498d
	github.com/gobwas/httphead v0.0.0-20180130184737-2c6c146eadee // indirect
	github.com/gobwas/pool v0.2.0 // indirect
	github.com/gobwas/ws v1.0.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/gopherjs/gopherjs v0.0.0-20190411002643-bd77b112433e // indirect
	github.com/gopherjs/gopherwasm v1.1.0
	github.com/kidoman/embd v0.0.0-20170508013040-d3d8c0c5c68d
	github.com/pbnjay/pixfont v0.0.0-20190130005054-401bb7c6aee2
)
