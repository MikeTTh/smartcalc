﻿# Project plan

### Muveletek
- [ ] atlag
- [ ] varhato ertek
- [ ] sorozatok? //mertani osszeg hianyzik meg
- [ ] Venn diagram?
- [x] Másodfok
- [ ] gyokkereses, masod es magasabb foku egyenletek megoldasa
- [ ] terulet, kerulet
- [ ] szogek, oldalak
- [ ] kor
- [ ] testek felszine, terfogata
- [ ] grafok?
- [x] alapmuveletek
- [ ] fuggvenyek info
- [ ] statisztika, grafikon rajzolas
- [ ] valoszinuseg szamitas
- [ ] integralas, derivalas
- [ ] fuggveny rajzolas
- [ ] szazalek szamitas
- [ ] tortek bevietele, atalkitasa tizedes tortekke
- [ ] kerekites n tizedesjegyre
- [ ] egyenletrendszerek, egyenlotlensegek megoldasa
- [ ] logaritmus 

### Egyeb
- [ ] memoria?
- [ ] Ui, leirasok legalabb a szukseges parameterekhez
- [ ] keplet, tetel tar? clear text, pdf tarolas?
- [ ] gyors keresheto kepletek?
- [ ] az adott muvelet eredmenye automatikusan menjen egy adott memoria slot(ok?)ba?
- [ ] valami instant off/clear gomb ami normal modba valt?
- [ ] mertekegyseg atvalto

Minden pipa elott sikeres teszt?

