package main

import (
	"../src/functions"
	"fmt"
)

func printResult(triangle functions.Haromszog) {
	fmt.Printf(
		`A oldal: %f,
B oldal: %f,
C oldal: %f,
Alfa szög: %f,
Beta szög: %f,
Gamma szög: %f,
Terület: %f,
Kerület: %f,
A magassag: %f,
B magassag: %f,
C magassag: %f,
R köré: %f,
R be: %f,

`,
		triangle.ALength,
		triangle.BLength,
		triangle.CLength,
		triangle.Alfa,
		triangle.Beta,
		triangle.Gamma,
		triangle.Terulet,
		triangle.Kerulet,
		triangle.AMagassag,
		triangle.BMagassag,
		triangle.CMagassag,
		triangle.RKoreIrhato,
		triangle.RBeirhato,
	)
}

func main() {
	var triangle1 functions.Haromszog
	triangle1.ALength = 6
	triangle1.BLength = 6
	triangle1.CLength = 6
	triangle1.Megold()
	printResult(triangle1)
	var triangle2 functions.Haromszog
	triangle2.ALength = 6
	triangle2.BLength = 6
	triangle2.Gamma = functions.ConvertDegreeToRadian(60)
	triangle2.Megold()
	printResult(triangle2)
	var triangle3 functions.Haromszog
	triangle3.ALength = 6
	triangle3.BLength = 6
	triangle3.Kerulet = 18
	triangle3.Megold()
	printResult(triangle3)
	var triangle4 functions.Haromszog
	triangle4.ALength = 6
	triangle4.BLength = 6
	triangle4.Terulet = 15.58846
	triangle4.Megold()
	printResult(triangle4)
	var triangle5 functions.Haromszog
	triangle5.ALength = 6
	triangle5.Alfa = functions.ConvertDegreeToRadian(60)
	triangle5.Beta = functions.ConvertDegreeToRadian(60)
	triangle5.Megold()
	printResult(triangle5)
}
