package output

type ScreenBorder struct {
	Module
	Border int
	Child  Module
}

func (s *ScreenBorder) Calc() []rune {
	return s.Child.Calc()
}

func (s *ScreenBorder) Draw() [][]bool {
	scr := [][]bool{}
	childscr := s.Child.Draw()
	y := len(childscr[0])
	for i := 0; i < s.Border; i++ {
		empty := make([]bool, y)
		scr = append(scr, empty)
	}
	for _, row := range childscr {
		rowout := []bool{}
		for i := 0; i < s.Border; i++ {
			rowout = append(rowout, false)
		}
		for _, px := range row {
			rowout = append(rowout, px)
		}
		scr = append(scr, rowout)
	}
	return scr
}
