package output

type Fn struct {
	Module
	Name  string
	Child Module
}

func (f *Fn) Calc() []rune {
	return []rune(f.Name + "(" + string(f.Child.Calc()) + ")")
}
