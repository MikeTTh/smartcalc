// +build js

package output

import (
	"sync"
	"syscall/js"
)

func Draw(img *[128][64]bool) {
	out := make([]byte, 256*128*4)
	for x, row := range *img {
		for y, c := range row {
			var col byte = 255
			if c {
				col = 0
			}
			for i := 0; i < 2; i++ {
				for j := 0; j < 2; j++ {
					out[((y*2+i)*256+x*2+j)*4] = col
					out[((y*2+i)*256+x*2+j)*4+1] = col
					out[((y*2+i)*256+x*2+j)*4+2] = col
					out[((y*2+i)*256+x*2+j)*4+3] = 255
				}
			}
		}
	}
	y := js.TypedArrayOf(out)
	go func() {
		js.Global().Call("drawCanvas", y)
		y.Release()
	}()
}

func loopy(this js.Value, args []js.Value) interface{} {
	Loopy()
	return nil
}

func Init() {
	js.Global().Call("setInterval", js.FuncOf(loopy), "1000")

	btnInit()

	// Idle our app, this way the browser doesn't close the Wasm VM
	var wait sync.WaitGroup
	wait.Add(1)
	wait.Wait()
}

func btnInit() {
	btns := js.Global().Get("document").Call("getElementsByTagName", "button")
	btnKeys := js.Global().Get("Object").Call("keys", btns)
	fndBtn := func(name string) func() {
		for _, b := range Btns {
			if name == b.Name {
				return b.Press
			}
		}
		return func() {}
	}
	btnKeys.Call("forEach", js.FuncOf(func(this js.Value, i []js.Value) interface{} {
		btn := btns.Call("item", i[0])
		t := btn.Get("textContent").String()

		btnfunc := fndBtn(t)

		btn.Call("addEventListener", "click", js.FuncOf(func(this js.Value, values []js.Value) interface{} {
			btnfunc()
			return nil
		}))

		return nil
	}))

	div := js.Global().Get("document").Call("getElementById", "buttons")

	addAButton := func(name string, press func()) {
		b := js.Global().Get("document").Call("createElement", "button")
		b.Set("innerText", name)
		b.Call("addEventListener", "click", js.FuncOf(func(this js.Value, values []js.Value) interface{} {
			press()
			return nil
		}))
		div.Call("appendChild", b)
	}
	BtnAdder(addAButton)
}
