package output

import "strings"

var replacelist = map[string]string{
	"°": "*deg",
	"π": "pi",
}

func fstr(in string) string {
	for a, b := range replacelist {
		in = strings.Replace(in, a, b, -1)
	}
	return in
}
