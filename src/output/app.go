// +build !js,!arm

package output

import (
	"fmt"
	"github.com/andlabs/ui"
	"log"
	"time"
)

var area *ui.Area
var brush *ui.DrawBrush
var screen *[128][64]bool

type areaHandler struct {
	ui.AreaHandler
}

func (areaHandler) Draw(a *ui.Area, p *ui.AreaDrawParams) {
	if screen == nil {
		return
	}
	brush.R, brush.G, brush.B, brush.A = 1, 1, 1, 1

	path := ui.DrawNewPath(ui.DrawFillModeWinding)
	path.AddRectangle(0, 0, p.AreaWidth, p.AreaHeight)
	path.End()
	p.Context.Fill(path, brush)
	path.Free()

	brush.R, brush.G, brush.B, brush.A = 0, 0, 0, 1

	scaleX := float64(len(*screen)-1) / p.AreaWidth
	scaleY := float64(len((*screen)[1])-1) / p.AreaHeight

	for x := 0.0; x < p.AreaWidth; x++ {
		for y := 0.0; y < p.AreaHeight; y++ {
			if (*screen)[int(x*scaleX)][int(y*scaleY)] {
				pa := ui.DrawNewPath(ui.DrawFillModeWinding)
				pa.AddRectangle(float64(x), float64(y), 1, 1)
				pa.End()
				p.Context.Fill(pa, brush)
				pa.Free()
			}
		}
	}
}

func (areaHandler) MouseEvent(a *ui.Area, me *ui.AreaMouseEvent) {
	// do nothing
}

func (areaHandler) MouseCrossed(a *ui.Area, left bool) {
	// do nothing
}

func (areaHandler) DragBroken(a *ui.Area) {
	// do nothing
}

func (areaHandler) KeyEvent(a *ui.Area, ke *ui.AreaKeyEvent) (handled bool) {
	// reject all keys
	switch ke.Key {
	case 10:
		(BtnsHelper["="]).Press()
		break
	default:
		fmt.Println(ke.Key)
		break
	}
	return false
}

func setupUI() {
	// set up brush
	brush = new(ui.DrawBrush)
	brush.Type = ui.DrawBrushTypeSolid
	brush.R, brush.G, brush.B, brush.A = 1, 1, 1, 1

	// set up window
	win := ui.NewWindow("calc", 400, 200, true)
	win.SetMargined(true)
	win.OnClosing(func(window *ui.Window) bool {
		win.Destroy()
		ui.Quit()
		return false
	})
	ui.OnShouldQuit(func() bool {
		win.Destroy()
		return true
	})
	vbox := ui.NewVerticalBox()
	area = ui.NewArea(areaHandler{})
	vbox.Append(area, true)

	// set up buttons
	hbox := ui.NewHorizontalBox()
	addABtn := func(name string, press func()) {
		b := ui.NewButton(name)
		b.OnClicked(func(button *ui.Button) {
			press()
		})
		hbox.Append(b, false)
	}
	BtnAdder(addABtn)
	vbox.Append(hbox, false)

	//show the window
	win.SetChild(vbox)
	win.Show()
}

func Init() {
	go func() {
		for true {
			go Loopy()
			time.Sleep(1 * time.Second)
		}
	}()
	err := ui.Main(setupUI)
	if err != nil {
		log.Fatal(err)
	}
}

func Draw(scr *[128][64]bool) {
	screen = scr
	if area != nil {
		area.QueueRedrawAll()
	}
}
