package output

type Fraction struct {
	Module
	Children []Module
	Select   int
}

func (f *Fraction) Value() *[]rune {
	return nil
}

func (f *Fraction) Selected() *int {
	return &f.Select
}

func (f *Fraction) ChildPtr(i int) *Module {
	return &f.Children[i]
}

func (f *Fraction) GetChildren() *[]Module {
	return &f.Children
}

func (f *Fraction) Calc() []rune {
	return append([]rune("(("), append(f.Children[0].Calc(), append([]rune(")/("), append(f.Children[1].Calc(), []rune("))")...)...)...)...)
}

func ifExists(s [][]bool, x int, y int) bool {
	if x < 0 {
		return false
	}
	if len(s) > x {
		if len(s[x]) > y {
			return s[x][y]
		}
	}
	return false
}

func (f *Fraction) Draw() [][]bool {
	scr := [][]bool{}
	xmax := 0
	// ymax := 0
	ch0 := f.Children[0].Draw()
	ch1 := f.Children[1].Draw()
	x0, y0 := MaxSize(ch0)
	x1, y1 := MaxSize(ch1)
	if x0 > x1 {
		xmax = x0
	} else {
		xmax = x1
	}

	pad0 := (xmax - x0) / 2
	pad1 := (xmax - x1) / 2
	/*if y0 > y1 {
		ymax = y0
	} else {
		ymax = y1
	}*/

	for x := 0; x < 2; x++ {
		col := make([]bool, 0)
		scr = append(scr, col)
	}

	for x := 0; x < xmax; x++ {
		col := []bool{}
		for y := 0; y < y0; y++ {
			col = append(col, ifExists(ch0, x-pad0, y))
		}
		col = append(col, false)
		col = append(col, true)
		for y := 0; y < y1; y++ {
			col = append(col, ifExists(ch1, x-pad1, y))
		}
		scr = append(scr, col)
	}

	for x := 0; x < 2; x++ {
		col := make([]bool, 0)
		scr = append(scr, col)
	}

	return scr
}
