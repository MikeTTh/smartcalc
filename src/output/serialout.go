// +build !js, arm

package output

import (
	"bytes"
	"sync"
	"time"

	"github.com/kidoman/embd"
	_ "github.com/kidoman/embd/host/all"
)

var (
	dc   = 6
	res  = 13
	led  = 24
	mode = true // LCD mode (true = data, false = cmd)
	spi  embd.SPIBus
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func writeCmd(b byte) {
	if mode {
		check(embd.DigitalWrite(dc, embd.Low))
		mode = false
	}
	_, e := spi.TransferAndReceiveByte(b)
	check(e)
}

func writeData(b []byte) {
	if !mode {
		check(embd.DigitalWrite(dc, embd.High))
		mode = true
	}
	for _, by := range b {
		_, e := spi.TransferAndReceiveByte(by)
		check(e)
	}
}

func setPos(x, y int) {
	writeCmd(0xb0 | byte(y))
	writeCmd(0x10 | (byte(x) >> 4))
	writeCmd(0x00 | (byte(x) & 0xf))
}

func empty() {
	var b []byte
	b = append(b, 0x00)
	b = bytes.Repeat(b, 128)
	for y := 0; y < 8; y++ {
		setPos(0, y)
		writeData(b)
	}
}

func Init() {
	// rpi setup
	check(embd.InitGPIO())
	check(embd.InitSPI())
	defer check(embd.CloseGPIO())
	defer check(embd.CloseSPI())
	// TODO: Create shutdown routine
	spi = embd.NewSPIBus(embd.SPIMode0, 0, 4000000, 8, 0)
	check(embd.SetDirection(dc, embd.Out))
	check(embd.SetDirection(res, embd.Out))
	check(embd.SetDirection(led, embd.Out))
	check(embd.DigitalWrite(dc, embd.High)) // Just so it is in sync with `mode`

	// LCD setup
	check(embd.DigitalWrite(res, embd.High))
	time.Sleep(50000 * time.Microsecond)
	check(embd.DigitalWrite(res, embd.Low))
	time.Sleep(5000 * time.Microsecond)
	check(embd.DigitalWrite(res, embd.High))
	check(embd.DigitalWrite(led, embd.High)) // Turn on backlight
	writeCmd(0xe2)
	writeCmd(0x40)
	writeCmd(0xa0)
	writeCmd(0xc8)
	writeCmd(0xa2)
	writeCmd(0x2c)
	writeCmd(0x2e)
	writeCmd(0x2f)
	writeCmd(0xf8)
	writeCmd(0x00)
	writeCmd(0x23)
	writeCmd(0x81)
	writeCmd(0x28)
	writeCmd(0xac)
	writeCmd(0x00)
	writeCmd(0xa6)
	writeCmd(0xaf)
	time.Sleep(100000 * time.Microsecond)
	writeCmd(0xa5)
	time.Sleep(200000 * time.Microsecond)
	writeCmd(0xa4)
	empty()

	go func() {
		for true {
			go Loopy()
			time.Sleep(1 * time.Second)
		}
	}()

	var wait sync.WaitGroup
	wait.Add(1)
	wait.Wait()
}

func getPx(s *[128][64]bool, x, y int) bool {
	if x < 0 {
		return false
	}
	if len(s) > x {
		if len(s[x]) > y {
			return s[x][y]
		}
	}
	return false
}

func Draw(scr *[128][64]bool) {
	for bank := 0; bank < 8; bank++ {
		setPos(0, bank)
		var data []byte
		for col := 0; col < 128; col++ {
			var b byte = 0x0
			for px := 0; px < 8; px++ {
				y := bank*8 + px
				val := getPx(scr, col, y)
				if val {
					b += 0x1 << uint(px)
				}
			}
			// writeData([]byte{b})
			data = append(data, b)
		}
		writeData(data)
	}
}
