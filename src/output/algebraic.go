package output

type Algebraic struct {
	Module
	Children []Module
	MaxSize  []int
	Select   int
}

func (a *Algebraic) Value() *[]rune {
	return nil
}

func (a *Algebraic) ChildPtr(i int) *Module {
	return &a.Children[i]
}

func (a *Algebraic) GetChildren() *[]Module {
	return &(a.Children)
}

func (a *Algebraic) Selected() *int {
	return &a.Select
}

func (a *Algebraic) Calc() []rune {
	r := []rune{}
	for _, c := range a.Children {
		r = append(r, c.Calc()...)
	}
	return r
}

func (a *Algebraic) Draw() [][]bool {
	canvas := [][]bool{}

	drawings := [][][]bool{}
	sizes := []int{}
	maxY := 0

	for _, c := range a.Children {
		d := c.Draw()
		drawings = append(drawings, d)
		_, y := MaxSize(d)
		sizes = append(sizes, y)
		if y > maxY {
			maxY = y
		}
	}

	for i, d := range drawings {
		pad := (maxY - sizes[i]) / 2
		for _, col := range d {
			padd := make([]bool, pad)
			padded := append(padd, col...)
			canvas = append(canvas, padded)
		}
	}

	return canvas
}

type AlgebraicWithSolution struct {
	Module
	Child   *Algebraic
	Sol     *Num
	MaxSize []int
}

func (a *AlgebraicWithSolution) Init(x, y int) {
	a.MaxSize = []int{x, y}
	a.Child = &Algebraic{
		MaxSize: []int{
			a.MaxSize[0],
			a.MaxSize[1] - 10,
		},
	}
	a.Sol = &Num{
		Val: []rune("="),
	}
}

func (a *AlgebraicWithSolution) Calc() []rune {
	return a.Child.Calc()
}

func (a *AlgebraicWithSolution) Draw() [][]bool {
	scr := [][]bool{}

	sol := a.Sol.Draw()
	aSolMaxX := a.MaxSize[0]
	aSolMaxY := a.MaxSize[1]
	_, solMaxY := MaxSize(sol)

	a.Child.MaxSize = []int{
		a.MaxSize[0],
		a.MaxSize[1] - solMaxY,
	}

	alg := a.Child.Draw()
	// algMaxX := a.Child.MaxSize[0]
	algMaxY := a.Child.MaxSize[1]
	for x := 0; x <= aSolMaxX; x++ {
		col := make([]bool, aSolMaxY)
		for y := 0; y < aSolMaxY; y++ {
			if y <= algMaxY {
				col[y] = ifExists(alg, x, y)
			} else {
				col[y] = ifExists(sol, x, y-algMaxY-1)
			}
		}
		scr = append(scr, col)
	}
	return scr
}
