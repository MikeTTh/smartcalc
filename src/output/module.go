package output

type Module interface {
	Draw() [][]bool
	Calc() []rune
	GetChildren() *[]Module
	ChildPtr(int) *Module
	Selected() *int
	Value() *[]rune
}

type BaseModule struct {
	Module
}

func MaxSize(d [][]bool) (int, int) {
	xmax := 0
	ymax := 0
	for x, r := range d {
		if x > xmax {
			xmax = x
		}
		y := len(r) - 1
		if y > ymax {
			ymax = y
		}
	}
	return xmax + 1, ymax + 1
}

func (m *BaseModule) Size() (int, int) {
	return MaxSize(m.Draw())
}

/*type Module struct {
	Name string
	Vars []interface{}
}

func (a *Module) calc() float64 {
	return math.NaN()
}

func (a *Module) draw() [][]bool {
	return [][]bool{}
}

func (a *Module) up() bool {
	return true
}

func (a *Module) down() bool {
	return true
}

func (a *Module) left() bool {
	return true
}

func (a *Module) right() bool {
	return true
}*/
