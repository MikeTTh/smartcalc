package output

import (
	"fmt"
	"github.com/Knetic/govaluate"
	"math"
)

type Button struct {
	Name  string
	Press func()
}

/*
TODO:
 * Make directional buttons actually work
*/

var BtnsHelper = make(map[string]Button)

var Btns = []Button{
	{
		"=",
		func() {
			defer func() {
				if err := recover(); err != nil {
					fmt.Println(err)
					sol.Val = []rune(GetErrorMsg())
					// pos = start
					DrawScr()
				}
			}()
			l := fstr(string((*algebraic).Calc()))
			fmt.Println(l)
			expr, e1 := govaluate.NewEvaluableExpressionWithFunctions(l, funcs)
			// TODO: fix expr being nil killing the function below (better error handling)
			res, e2 := expr.Evaluate(params)
			if e1 == nil && e2 == nil && !math.IsNaN(res.(float64)) {
				params["Ans"] = res
				sols := fmt.Sprintf("=%f", res)
				sol.Val = []rune(sols)
			} else {
				fmt.Println(e1)
				fmt.Println(e2)
				sol.Val = []rune(GetErrorMsg())
			}
			// pos = start
			DrawScr()
		},
	},
	{
		"Shift",
		func() {
			shift = !shift
		},
	},
	{
		"Alpha",
		func() {
			alpha = !alpha
		},
	},
	{
		"x/y",
		func() {
			var m Module = algebraic
			var sel *Module = &m
			var parent *Module
			for (*sel).Value() == nil {
				children := (*sel).GetChildren()
				if len(*children) != 0 {
					parent = sel
					sel = (*sel).ChildPtr(*(*sel).Selected()) // Pointer hell
				} else {
					break
				}
			}
			children := (*parent).GetChildren()
			*children = append(*children, &Fraction{
				Children: []Module{
					&Algebraic{},
					&Algebraic{},
				},
			})
		},
	},
}

func init() {
	for _, b := range Btns {
		BtnsHelper[b.Name] = b
	}
}

func BtnAdder(f func(name string, press func())) {
	for _, b := range Btns {
		f(b.Name, b.Press)
	}
}
