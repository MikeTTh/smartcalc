package output

import (
	"github.com/Knetic/govaluate"
	"math"
)

var params = map[string]interface{}{
	"pi":  math.Pi,
	"Pi":  math.Pi,
	"π":   math.Pi,
	"°":   math.Pi / 180,
	"deg": math.Pi / 180,
}

var funcs = map[string]govaluate.ExpressionFunction{
	"sqrt": func(args ...interface{}) (interface{}, error) {
		return math.Sqrt(args[0].(float64)), nil
	},
	"sin": func(args ...interface{}) (interface{}, error) {
		return math.Sin(args[0].(float64)), nil
	},
	"cos": func(args ...interface{}) (interface{}, error) {
		return math.Cos(args[0].(float64)), nil
	},
	"tan": func(args ...interface{}) (interface{}, error) {
		return math.Tan(args[0].(float64)), nil
	},
	"pow": func(args ...interface{}) (interface{}, error) {
		return math.Pow(args[0].(float64), args[1].(float64)), nil
	},
}
