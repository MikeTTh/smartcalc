package output

import (
	"math/rand"
)

var scr [128][64]bool
var start = [2]int{2, 2}
var pos = start
var linepos = 0

// var blink = true
var line = []rune("")
var shift = false
var alpha = false
var bord = &ScreenBorder{}
var algebraic *Algebraic
var sol *Num

func init() {
	algebraic = &Algebraic{}
	algebraic.Children = []Module{
		&Fraction{
			Children: []Module{
				&Fraction{
					Children: []Module{
						&Algebraic{
							Children: []Module{
								&Num{
									Val: []rune("3+1"),
								},
							},
						},
						&Num{
							Val: []rune("2"),
						},
					},
				},
				&Num{
					Val: []rune("5"),
				},
			},
		},
		&Num{
			Val: []rune("*10"),
		},
	}
	bord.Border = 2
	asol := &AlgebraicWithSolution{}
	asol.Init(len(scr)-bord.Border*2, len(scr[0])-bord.Border*2)
	asol.Child = algebraic
	sol = asol.Sol
	bord.Child = asol
}

func Blink() {
	// TODO: Implement blink?
}

func DrawScr() {
	screen := [128][64]bool{}
	for x, row := range bord.Draw() {
		for y, px := range row {
			screen[x][y] = px
		}
	}
	Draw(&screen)
}

func Loopy() {
	// Blink()
	DrawScr()
}

func GetErrorMsg() string {
	errors := []string{
		"oh shit",
		"dammit",
		"error",
	}
	n := rand.Intn(len(errors) - 1)
	return errors[n]
}
