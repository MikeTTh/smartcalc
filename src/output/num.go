package output

/**
TODO:
 * Rename to Text
 * Use actual fonts instead of a 2d bool slice
*/

import (
	"gitlab.com/MikeTTh/smartcalc/src/letters"
)

type Num struct {
	Module
	Val    []rune
	Select int
}

func (n *Num) Selected() *int {
	return &n.Select
}

func (n *Num) Calc() []rune {
	return n.Val
}

func (n *Num) Value() *[]rune {
	return &n.Val
}

func (n *Num) Draw() [][]bool {
	out := [][]bool{}
	empty := make([]bool, 8)
	out = append(out, empty)
	out = append(out, empty)
	for _, r := range n.Val {
		c := letters.Letters[string(r)]
		for _, row := range c {
			rowOut := []bool{}
			rowOut = append(rowOut, false)
			for _, px := range row {
				rowOut = append(rowOut, px)
			}
			// rowOut = append(rowOut, false)
			out = append(out, rowOut)
		}
	}
	return out
}
