package output

import "fmt"

func Dec2frac(d float64) string {
	df := 1.0
	top := 1
	bot := 1

	for df != d {
		if df < d {
			top++
		} else {
			bot++
			top = int(d * float64(bot))
		}
		df = float64(top) / float64(bot)
	}
	return fmt.Sprintf("%d/%d", top, bot)
}
