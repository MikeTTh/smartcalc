package functions

import "math"

func SorozatSzamtaniOsszeg(a1 float64, d float64, n float64) float64 {
	return ((2*a1 + (n-1)*d) * n) / 2
}

func SorozatSzamtaniNtag(a1 float64, d float64, n float64) float64 {
	return a1 + (d * (n - 1))
}

func SorozatMertaniNtag(a1 float64, q float64, n float64) float64 {
	return a1 * math.Pow(q, (n-1))
}

func SorozatMertaniOsszeg(a1 float64, q float64, n float64) float64 {
	return a1 * (math.Pow(q, n) - 1) / (q - 1)
}
