package functions

import (
	"math"
)

// a oldallal szemben mindig alfa szog van, b vel beta, c vel gamma
// a szogek radianban vannak
type Haromszog struct {
	ALength float64
	BLength float64
	CLength float64

	// RADIÁN
	Alfa  float64
	Beta  float64
	Gamma float64

	AMagassag float64
	BMagassag float64
	CMagassag float64

	Terulet float64
	Kerulet float64

	RKoreIrhato float64
	RBeirhato   float64
}

func (h *Haromszog) Megold() {
	// Ha megvan a 3 oldal, akkor tudjuk a kerületet és Heron képlettel kijön a terület
	if h.ALength != 0 && h.BLength != 0 && h.CLength != 0 && h.Terulet == 0 {
		h.Kerulet = h.ALength + h.BLength + h.CLength
		s := h.Kerulet / 2
		h.Terulet = math.Sqrt(s * (s - h.ALength) * (s - h.BLength) * (s - h.CLength))
		h.RBeirhato = h.Terulet / s
		h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
		h.Alfa = math.Asin(h.ALength / (2 * h.RKoreIrhato))
		h.Beta = math.Asin(h.BLength / (2 * h.RKoreIrhato))
		h.Gamma = math.Asin(h.CLength / (2 * h.RKoreIrhato))
		h.AMagassag = 2 * h.Terulet / h.ALength
		h.BMagassag = 2 * h.Terulet / h.BLength
		h.CMagassag = 2 * h.Terulet / h.CLength
		return // mert megvan minden
	}

	// Ha megvan 2 oldal es a koztuk levo szog, szinuszos teruletkeplet
	if h.ALength != 0 && h.BLength != 0 && h.Gamma != 0 && h.Terulet == 0 {
		h.Terulet = (h.ALength * h.BLength * math.Sin(h.Gamma)) / 2
		// Csak akkor szamoljuk ki a keruletet ha meg nem tudjuk
		if h.Kerulet == 0 {
			// A koszinusz tetellel kiszamolhato a harmadik oldal es abbol a kerulet
			h.CLength = math.Sqrt(math.Pow(h.ALength, 2) + math.Pow(h.BLength, 2) - 2*h.ALength*h.BLength*math.Cos(h.Gamma))
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.Alfa = math.Asin(h.ALength / (2 * h.RKoreIrhato))
			h.Beta = math.Asin(h.BLength / (2 * h.RKoreIrhato))
			h.Gamma = math.Asin(h.CLength / (2 * h.RKoreIrhato))
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
		}
		return
	}
	if h.BLength != 0 && h.CLength != 0 && h.Alfa != 0 && h.Terulet == 0 {
		h.Terulet = (h.BLength * h.CLength * math.Sin(h.Alfa)) / 2
		if h.Kerulet == 0 {
			h.ALength = math.Sqrt(math.Pow(h.BLength, 2) + math.Pow(h.CLength, 2) - 2*h.BLength*h.CLength*math.Cos(h.Alfa))
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.Alfa = math.Asin(h.ALength / (2 * h.RKoreIrhato))
			h.Beta = math.Asin(h.BLength / (2 * h.RKoreIrhato))
			h.Gamma = math.Asin(h.CLength / (2 * h.RKoreIrhato))
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
		}
		return
	}
	if h.CLength != 0 && h.ALength != 0 && h.Beta != 0 && h.Terulet == 0 {
		h.Terulet = (h.CLength * h.ALength * math.Sin(h.Beta)) / 2
		if h.Kerulet == 0 {
			h.BLength = math.Sqrt((math.Pow(h.CLength, 2) + math.Pow(h.ALength, 2)) - 2*h.CLength*h.ALength*math.Cos(h.Beta))
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.Alfa = math.Asin(h.ALength / (2 * h.RKoreIrhato))
			h.Beta = math.Asin(h.BLength / (2 * h.RKoreIrhato))
			h.Gamma = math.Asin(h.CLength / (2 * h.RKoreIrhato))
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
		}
		return
	}

	// 2 oldal + kerület
	// Külön blokk, így ha véget ér, az ideiglenes változók törlődnek RAM-ból
	oldalak := []float64{h.ALength, h.BLength, h.CLength}
	oldalszam := 0
	osszeg := 0.0
	hianyoldal := -1
	for i, o := range oldalak {
		if o != 0 {
			oldalszam++
			osszeg += o
		} else {
			hianyoldal = i
		}
		if oldalszam == 2 && h.Kerulet != 0 {
			oldal := h.Kerulet - osszeg
			switch hianyoldal {
			case 0:
				h.ALength = oldal
				break
			case 1:
				h.BLength = oldal
				break
			case 2:
				h.CLength = oldal
				break
			}
			h.Megold()
		}
	}

	// Ha megvan 1 oldal es a hozza tartozo magassag
	if h.ALength != 0 && h.AMagassag != 0 && h.Terulet == 0 {
		h.Terulet = (h.ALength * h.AMagassag) / 2
	}
	if h.BLength != 0 && h.BMagassag != 0 && h.Terulet == 0 {
		h.Terulet = (h.BLength * h.BMagassag) / 2
	}
	if h.CLength != 0 && h.CMagassag != 0 && h.Terulet == 0 {
		h.Terulet = (h.CLength * h.CMagassag) / 2
	}

	// Beirhato koros teruletkeplet
	if h.Kerulet != 0 && h.RBeirhato != 0 && h.Terulet == 0 {
		if h.Kerulet != 0 {
			h.Terulet = (h.Kerulet / 2) * h.RBeirhato
		}
	}

	// 2 szog es terulet
	// 2 szog es kerulet

	// 2 szog es 1 oldal
	if h.Alfa != 0 && h.Beta != 0 && (h.ALength != 0 || h.BLength != 0) {
		h.Gamma = 180 - h.Alfa - h.Beta
		if h.ALength != 0 {
			h.BLength = (math.Sin(h.Beta) / math.Sin(h.Alfa)) * h.ALength
			h.CLength = (math.Sin(h.Gamma) / math.Sin(h.Beta)) * h.BLength
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
			return
		}
		if h.BLength != 0 {
			h.ALength = (math.Sin(h.Alfa) / math.Sin(h.Beta)) * h.BLength
			h.CLength = (math.Sin(h.Gamma) / math.Sin(h.Alfa)) * h.ALength
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
			return
		}
	}

	if h.Beta != 0 && h.Gamma != 0 && (h.BLength != 0 || h.CLength != 0) {
		h.Alfa = 180 - h.Beta - h.Gamma
		if h.BLength != 0 {
			h.CLength = (math.Sin(h.Gamma) / math.Sin(h.Beta)) * h.BLength
			h.ALength = (math.Sin(h.Alfa) / math.Sin(h.Gamma)) * h.CLength
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
			return
		}
		if h.CLength != 0 {
			h.BLength = (math.Sin(h.Beta) / math.Sin(h.Gamma)) * h.CLength
			h.ALength = (math.Sin(h.Alfa) / math.Sin(h.Beta)) * h.BLength
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
			return
		}
	}

	if h.Alfa != 0 && h.Gamma != 0 && (h.ALength != 0 || h.CLength != 0) {
		if h.ALength != 0 {
			h.CLength = (math.Sin(h.Gamma) / math.Sin(h.Alfa)) * h.ALength
			h.BLength = (math.Sin(h.Beta) / math.Sin(h.Gamma)) * h.CLength
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
			return
		}
		if h.CLength != 0 {
			h.ALength = (math.Sin(h.Alfa) / math.Sin(h.Gamma)) * h.CLength
			h.BLength = (math.Sin(h.Beta) / math.Sin(h.Alfa)) * h.ALength
			h.Kerulet = h.ALength + h.BLength + h.CLength
			s := h.Kerulet / 2
			h.RBeirhato = h.Terulet / s
			h.RKoreIrhato = (h.ALength * h.BLength * h.CLength) / (4 * h.Terulet)
			h.AMagassag = 2 * h.Terulet / h.ALength
			h.BMagassag = 2 * h.Terulet / h.BLength
			h.CMagassag = 2 * h.Terulet / h.CLength
			return
		}
	}
	// TODO Implement more scenarios.
}
