package functions

type Negyszog struct {
	ALength float64
	BLength float64
	CLength float64
	DLength float64

	Alfa  float64
	Beta  float64
	Gamma float64
}
