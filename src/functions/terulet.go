package functions

import "math"

func NegyzetTerulet(a float64) float64 {
	return math.Pow(a, 2)
}

func NegyszogTerulet(a float64, b float64) float64 {
	return a * b
}

func HaromszogTeruletAlapxMagassag(a float64, b float64) float64 {
	return a * b
}

func HaromszogTeruletSin(a float64, b float64, g float64) float64 {
	return (a * b * math.Sin(g)) / 2
}

func KorTerulet(r float64) float64 {
	return r * r * math.Pi
}

func HaromszogTeruletHeron(a float64, b float64, c float64) float64 {
	s := (a + b + c) / 2
	return math.Sqrt(s * (s - a) * (s - b) * (s - c))
}

func HaromszogTeruletDerekszogu(a float64, b float64) float64 {
	return (a * b) / 2
}
