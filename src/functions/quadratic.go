package functions

import "math"

type function struct {
}

func Quadratic(a, b, c float64) (float64, float64) {
	s := math.Sqrt(math.Pow(b, 2) - 4*a*c)
	x1 := (-b + s) / (2 * a)
	x2 := (-b - s) / (2 * a)
	return x1, x2
}
