package functions

import "math"

func NegyzetKerulet(a float64) float64 {
	return a * 4
}

func NegyszogKerulet(a float64, b float64) float64 {
	return a * b
}

func KorKerulet(r float64) float64 {
	return 2 * r * math.Pi
}

func HaromszogKeruletSzabalyos(a float64) float64 {
	return ((math.Sqrt(3)) / 2) * a
}

func HaromszogKeruletEgyenloSzaru(a float64, b float64) float64 {
	return 2*a + b
}
