package functions

import "math"

func ConvertDegreeToRadian(a float64) float64 {
	return (a * math.Pi) / 180
}

func ConvertRadianToDegree(a float64) float64 {
	return (a * 180) / math.Pi
}
