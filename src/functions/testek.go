package functions

import "math"

func TerfogatHasab(a float64, b float64, c float64) float64 {
	return a * b * c
}

func FelszinHasab(a float64, b float64, c float64) float64 {
	return (2 * a * b) + (2 * a * c) + (2 * b * c)
}

func TerfogatGula(T float64, M float64) float64 {
	return (T * M) / 3
}

func FelszinNegyzetalapuGula(a float64, m float64) float64 {
	return (a * a * m) / 3
}

func FelszinCsonkagula(T float64, t float64, P float64) float64 {
	return T + t + P
}

func TerfogatCsonkagula(m float64, T float64, t float64) float64 {
	return (m * (T + math.Sqrt(T*t) + t)) / 3
}
