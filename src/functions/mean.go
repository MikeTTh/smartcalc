package functions

func Atlag(be []float64) float64 {
	var ki float64 = 0
	for _, n := range be {
		ki += n
	}
	return ki / float64(len(be))
}
