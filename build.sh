#!/usr/bin/env bash

DIR=$(pwd)

echo Generating fonts
cd buildtools/fontextractor
mkdir -p out/go
go run main.go -go
cp out/go/letters.go $DIR/src/letters/letters.go
cd $DIR

echo Building WASM
cd src
GOOS=js GOARCH=wasm go build -o $DIR/build/index.wasm main.go
echo Building JS
$(go env GOPATH)/bin/gopherjs build -o $DIR/build/index.js main.go
#go run main.go

echo Building for Pi
GOOS=linux GOARCH=arm go build -o $DIR/build/picalc main.go