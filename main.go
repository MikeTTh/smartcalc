package main

import (
	"net/http"
	"os"
	"path/filepath"
	"fmt"
)

func findSourceFile(file string) string {
	out := ""
	err := filepath.Walk("src", func(path string, info os.FileInfo, err error) error {
		if info.Name() == file {
			out = path
			return nil
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	return out
}

func serve(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		http.ServeFile(w, r, "src/index.html")
		return
	}
	path := r.URL.Path[1:]
	if _, err := os.Stat("build/" + path); os.IsNotExist(err) {
		if filename := findSourceFile(path); filename != "" {
			http.ServeFile(w, r, filename)
			return
		} else {
			return
		}
	} else {
		http.ServeFile(w, r, "build/"+path)
		return
	}
}

func main() {
	http.HandleFunc("/", serve)
	fmt.Println("Running on localhost:8080")
	http.ListenAndServe(":8080", nil)
}
