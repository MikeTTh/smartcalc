# Számológép cuccos 🧮

## Struktúra

* `build`: ide kerülnek a megépített fileok, ezek nincsenek git-re szinkronizálva
* `src`: forráskód a tényleges számológéphez
* `main.go` (`src`-n kívüli): helyi webserver `localhost:8080`-on tesztelésre

### src mappa

* `common`: közös/alap dolgok helye. Ide fog kerülni a parser (ami *'átolvassa'* a beírt szöveget) is.
* `letters`: ide kerülnek a számokat, betűket tartalmazó fileok és íráshoz tartozó logika
* `operations`: műveletek (`+-*/`, stb) helye
* `functions`: olyan funkciók helye, amelyek külön menüt fognak kapni (mert GUI kell nekik vagy ilyesmi) pl: másodfok, ábrázolás, statisztika
* `output`: kimenetek kezelésének helye, egyelőre a webfelület van benne
* `ui`: menük, logók, stb. helye

## Project haladás
Lásd: [itt](project.md)


## Függőségek telepítése

* golang (linux: csomagkezelő, windows: letöltés)

## Építés, futtatás

* A webfelület gopherjs-el van lefordítva és a `build/index.js` helyre kell kerüljön
* A server közvetlenül futtatható
* Így linuxon:
    ```
    ./build.sh
    go run main.go
    ```
* Vagy ha a bash nem standard helyen van (Termux)
    ```
    bash build.sh
    go run main.go
    ```
* Windowson, mivel nem ismerem a PowerShell-t, így nincs automatizált build:
    * Válts az `src` mappába: `cd src`
    * Kérd le a Go-tól a telepített csomagok helyét: `go env GOPATH`
    * Futtasd a `gopherjs`-t, hogy megépítse a weblapot: `<előző parancs kimenete>\bin\gopherjs -o ..\build\index.js main.go`
    * Lépj vissza a fő mappába: `cd ..`
    * Futtasd a szervert: `go run main.go`
    * Összegezve:
        ```
        cd src
        go env GOPATH
        <előző parancs kimenete>\bin\gopherjs -o ..\build\index.js main.go
        cd ..
        go run main.go
        ```